package exceptions;

import java.util.List;

public class NegativeNumberInputException extends Exception {

    public static final String ERR_STRING = "Negatives not allowed: ";

    public NegativeNumberInputException(List<String> negatives) {
        super(ERR_STRING + String.join(",",negatives));
    }
}
