import exceptions.NegativeNumberInputException;

import java.util.ArrayList;
import java.util.List;

public class StringCalculator {

    char[] escapedRegexChars = {'.','*','\\','[',']','^','+','-','?','$'};

    public int add(String numbers) throws NegativeNumberInputException {
        int sum = 0;
        List<String> negatives = new ArrayList<>();
        try {
            ArrayList<String> delimitters = getApplicableDelimitters(numbers);
            if(numbers.startsWith("//")) {
                // Remove the first line containing delimitters from input
                int delimitterEndIndex = numbers.indexOf("\n");
                numbers = numbers.substring(delimitterEndIndex+1);
            }
            String delimitterRegex = String.join("|", delimitters);
            String[] inputNumberStrings = numbers.split(delimitterRegex);

            for(String inputNumberString: inputNumberStrings) {
                int number = Integer.parseInt(inputNumberString);
                if(number < 0) {
                    negatives.add(inputNumberString);
                } else if(number <= 1000) {
                    sum += number;
                }
            }
        } catch (NumberFormatException e) {
            System.out.println("Invalid number string. Failed execution");
        }
        if(negatives.size() > 0) {
            throw new NegativeNumberInputException(negatives);
        }
        return sum;
    }

    private ArrayList<String> getApplicableDelimitters(String input) {
        ArrayList<String> delimitters = new ArrayList<>();
        if(input.startsWith("//")) {
            // Delimitter supplied in the first line between // and \n
            int delimitterEndIndex = input.indexOf("\n");
            String delimitterString = input.substring(2, delimitterEndIndex) + "[";
            String[] delimitterPortions = delimitterString.split("]\\[");

            for(String delimitter: delimitterPortions) {
                if (delimitter.startsWith("[") && delimitter.length() > 1) {
                    // Remove brackets from delimitter
                    delimitter = delimitter.substring(1);
                }
                if(delimitter.endsWith("[") && delimitter.length() > 1) {
                    delimitter = delimitter.substring(0, delimitter.length() - 1);
                }
                StringBuilder sb = new StringBuilder();
                for (char c : delimitter.toCharArray()) {
                    if (isEscaped(c)) {
                        sb.append("\\");
                    }
                    sb.append(c);
                }
                delimitter = sb.toString();
                delimitters.add(delimitter);
            }
        } else {
            // Delimitters: \n or ,
            delimitters.add(",");
            delimitters.add("\n");
        }
        return delimitters;
    }

    private boolean isEscaped(char c) {
        for(char ch: escapedRegexChars) {
            if(c == ch) {
                return true;
            }
        }
        return false;
    }
}