import exceptions.NegativeNumberInputException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class StringCalculatorTests {
    @Test
    public void simpleStringCalculatorTest() {
        StringCalculator stringCalculator = new StringCalculator();
        try {
            assertEquals(0, stringCalculator.add(""));
            assertEquals(1, stringCalculator.add("1"));
            assertEquals(3, stringCalculator.add("1,2"));
        } catch (NegativeNumberInputException e) {
        }
    }

    @Test
    public void unknownAmountOfNumbersTest() {
        StringCalculator stringCalculator = new StringCalculator();
        try {
            assertEquals(6, stringCalculator.add("1,2,3"));
            assertEquals(35, stringCalculator.add("5,10,15,5"));
        } catch (NegativeNumberInputException e) {
        }
    }

    @Test
    public void newlineHandleTest() {
        StringCalculator stringCalculator = new StringCalculator();
        try {
            assertEquals(30, stringCalculator.add("10\n20"));
            assertEquals(6, stringCalculator.add("1,2\n3"));
        } catch (NegativeNumberInputException e) {
        }
    }

    @Test
    public void differentDelimitterTest() {
        StringCalculator stringCalculator = new StringCalculator();
        try {
            assertEquals(3, stringCalculator.add("//;\n1;2"));
            assertEquals(10, stringCalculator.add("//:\n2:3:5"));
        } catch (NegativeNumberInputException e) {
        }
    }

    @Test
    public void negativeNumberExceptionTest() {
        StringCalculator stringCalculator = new StringCalculator();
        Throwable exception1 = assertThrows(NegativeNumberInputException.class, () -> stringCalculator.add("-1,2"));
        assertEquals(NegativeNumberInputException.ERR_STRING + "-1", exception1.getMessage());
        Throwable exception2 = assertThrows(NegativeNumberInputException.class, () -> stringCalculator.add("10,-2,-5,20"));
        assertEquals(NegativeNumberInputException.ERR_STRING + "-2,-5", exception2.getMessage());
    }

    @Test
    public void largeNumberIgnoreTest() {
        StringCalculator stringCalculator = new StringCalculator();
        try {
            assertEquals(2, stringCalculator.add("2,1001"));
            assertEquals(10, stringCalculator.add("1,2,3\n4,5000,10000"));
        } catch (NegativeNumberInputException e) {
        }
    }

    @Test
    public void multiCharDelimitterTest() {
        StringCalculator stringCalculator = new StringCalculator();
        try {
            assertEquals(6, stringCalculator.add("//[***]\n1***2***3"));
            assertEquals(10, stringCalculator.add("//[\n1[2[3[4"));
        } catch (NegativeNumberInputException e) {
        }
    }

    @Test
    public void multiDelmitterTest() {
        StringCalculator stringCalculator = new StringCalculator();
        try {
            assertEquals(6, stringCalculator.add("//[*][%]\n1*2%3"));
        } catch (NegativeNumberInputException e) {
        }
    }

    @Test
    public void variableLengthMultiDelimitterTest() {
        StringCalculator stringCalculator = new StringCalculator();
        try {
            assertEquals(10, stringCalculator.add("//[***][@][$$][%]\n1***2@3$$4"));
        } catch (NegativeNumberInputException e) {
        }
    }
}
